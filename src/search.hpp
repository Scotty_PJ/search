#include<iostream>
#include<string>

using namespace std;
/*
int search(auto text, auto pattern)
{
	//int pattern_size = pattern.size();
	int match = 0;
	int found = -1;
	
	for(int i = 0; i < text.size(); i++)
	{
		if (text[i] == pattern[0])
		{
			match++;
			int z = i + 1;
			
			for(int y = 1; y < pattern.size(); y++)
			{
				
				if (text[z] == pattern[y])
				{
					match++;
					z++;
				}
				else
				{
					match = 0;
					break;
				}
			}
			
			if (match == pattern.size())
			{
				found = i;
				break;
			}
		}
	}
	
	return found;
}*/


int search(auto text, auto pattern)
{
	
	int match = 0;
	
	for(int i = 0; i < text.size(); i++)
	{
		if (text[i] == pattern[0])
		{
			match++;
			int z = i + 1;
			
			for(int y = 1; y < pattern.size(); y++)
			{
				
				if (text[z] == pattern[y])
				{
					match++;
					z++;
				}
				else
				{
					match = 0;
					break;
				}
			}
			
			if (match == pattern.size())
			{
				return i;
			}
		}
	}
	
	return -1;
}
